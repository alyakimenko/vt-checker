package vt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
)

const (
	payloadMaxSize = 30 * 1024 * 1024 // 30 MB
)

type ReportResponse struct {
	Data struct {
		Attributes struct {
			Results map[string]struct {
				Category string `json:"category"`
				Result   string `json:"result"`
			} `json:"results"`
			Stats  map[string]int `json:"stats"`
			Status string         `json:"status"`
		} `json:"attributes"`
	} `json:"data"`
}

type Client struct {
	apiKey  string
	baseURL string
	agent   string
	client  *http.Client
}

func New(apiKey string) *Client {
	return &Client{
		apiKey:  apiKey,
		baseURL: "https://www.virustotal.com/api/v3/",
		client:  &http.Client{},
	}
}

func (c *Client) ScanFile(filename string, file io.Reader) (string, error) {
	b := bytes.Buffer{}

	w := multipart.NewWriter(&b)
	f, err := w.CreateFormFile("file", filename)
	if err != nil {
		return "", err
	}

	payloadSize, err := io.Copy(f, file)
	if err != nil {
		return "", err
	}

	w.Close()

	var uploadURL string
	if payloadSize > payloadMaxSize {
		uploadURL, err = c.getUploadURL()
		if err != nil {
			return "", err
		}
	} else {
		uploadURL = c.baseURL + "files"
	}

	headers := map[string]string{"Content-Type": w.FormDataContentType()}

	ungzipper, err := c.sendRequest("POST", uploadURL, &b, headers)
	if err != nil {
		return "", err
	}
	defer ungzipper.Close()

	respData := struct {
		Data struct {
			ID string `json:"id"`
		} `json:"data"`
	}{}

	if err := json.NewDecoder(ungzipper).Decode(&respData); err != nil {
		return "", err
	}

	return respData.Data.ID, err
}

func (c *Client) GetReport(fileID string) (*ReportResponse, error) {
	ungzipper, err := c.sendRequest("GET", fmt.Sprintf("%sanalyses/%s", c.baseURL, fileID), nil, nil)
	if err != nil {
		return nil, err
	}
	defer ungzipper.Close()

	var report ReportResponse

	if err := json.NewDecoder(ungzipper).Decode(&report); err != nil {
		return nil, err
	}

	return &report, nil
}
