package vt

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

const version = "0.3"

func (c *Client) sendRequest(method string, url string, body io.Reader, headers map[string]string) (*gzip.Reader, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	agent := c.agent
	if agent == "" {
		agent = "unknown"
	}

	req.Header.Set("User-Agent", fmt.Sprintf("%s; vtgo %s; gzip", agent, version))
	req.Header.Set("Accept-Encoding", "gzip")
	req.Header.Set("X-Apikey", c.apiKey)

	if headers != nil {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	ungzipper, err := gzip.NewReader(resp.Body)
	if err != nil {
		return nil, err
	}

	return ungzipper, nil
}

// getUploadURL returns a URL for a large file
func (c *Client) getUploadURL() (string, error) {
	req, err := http.NewRequest(http.MethodGet, c.baseURL+"files/upload_url", nil)
	if err != nil {
		return "", err
	}

	req.Header.Set("X-Apikey", c.apiKey)
	
	resp, err := c.client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	respBody := struct {
		Data string `json:"data"`
	}{}

	if err := json.Unmarshal(data, &respBody); err != nil {
		return "", err
	}

	return respBody.Data, nil
}
