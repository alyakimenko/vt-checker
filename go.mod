module gitlab.com/alyakimenko/vt-checker

go 1.14

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/gofiber/fiber/v2 v2.0.1
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
