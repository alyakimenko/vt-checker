package config

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	VirusTotalToken  string `json:"vt_token"` // x-apikey header
	Port             string `json:"port"`
	TelegramBotToken string `json:"bot_token"`
	ChatID           int64  `json:"chat_id"`
}

func ParseConfigFile(filename string) (*Config, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var config Config
	if err := json.Unmarshal(data, &config); err != nil {
		return nil, err
	}

	return &config, nil
}
