package workers

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/alyakimenko/vt-checker/config"
)

func ListenBotUpdated(cfg *config.Config, bot *tgbotapi.BotAPI) {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		log.Println(err)
	}

	for update := range updates {
		if update.Message == nil { 
			continue
		}

		command := update.Message.Command()
		
		if command != "start_check" {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		StartWorker(cfg, true)
	}

}
