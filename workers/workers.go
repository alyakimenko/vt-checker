package workers

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"strings"
	"time"

	"gitlab.com/alyakimenko/vt-checker/config"
)

const defaultTimout = 5 * time.Hour

var (
	filesURLs = []string{
		"https://updates.brovpn.io/BroVPN-Setup.exe",
		"https://updates.brovpn.io/BroVPN_SVC.exe",
		"https://updates.brovpn.io/BroVPN.exe",
	}
)

func StartWorker(config *config.Config, once bool) {
	for {
		for _, url := range filesURLs {
			splittedURL := strings.Split(url, "/")
			checkFiles(splittedURL[len(splittedURL)-1], url, config.Port)
		}

		if once {
			log.Println("Once is breaks")
			break
		}

		time.Sleep(defaultTimout)
	}
}

func checkFiles(filename, url, port string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return
	}
	defer resp.Body.Close()

	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return
	}

	reqBody := new(bytes.Buffer)
	multipartWriter := multipart.NewWriter(reqBody)
	part, err := multipartWriter.CreateFormFile("file", filename)
	if err != nil {
		log.Println(err)
		return
	}

	part.Write(respData)

	err = multipartWriter.Close()
	if err != nil {
		log.Println(err)
		return
	}

	_, err = http.Post(fmt.Sprintf("http://localhost%s/check", port), multipartWriter.FormDataContentType(), reqBody)
	if err != nil {
		log.Println(err)
		return
	}
}
