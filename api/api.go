package api

import (
	"errors"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/alyakimenko/vt-checker/config"
	"gitlab.com/alyakimenko/vt-checker/telegram"
	"gitlab.com/alyakimenko/vt-checker/vt"
)

type API struct {
	vtClient *vt.Client
	bot      *telegram.Bot
	config   *config.Config
}

func New(config *config.Config, bot *telegram.Bot) (*API, error) {
	return &API{
		vtClient: vt.New(config.VirusTotalToken),
		bot:      bot,
		config:   config,
	}, nil
}

func (api *API) Start() error {
	app := fiber.New(fiber.Config{
		BodyLimit: 200 * 1024 * 1024, // 200 Megabytes
	})

	app.Use(logger.New())
	app.Post("/check", api.checkFile)

	return app.Listen(api.config.Port)
}

func (api *API) checkFile(ctx *fiber.Ctx) error {
	fileHeader, err := ctx.FormFile("file")
	if err != nil {
		return err
	}

	if fileHeader.Size == 0 {
		return errors.New("file size is 0")
	}

	file, err := fileHeader.Open()
	if err != nil {
		return err
	}

	log.Printf("File size: %d, filename: %s", fileHeader.Size, fileHeader.Filename)

	go api.scanFile(fileHeader.Filename, file)

	return nil
}

func (api *API) scanFile(filename string, file io.Reader) {
	fileID, err := api.vtClient.ScanFile(filename, file)
	if err != nil {
		log.Printf("err: %v, filename: %s\n", err, filename)
		return
	}

	log.Println("Scanning file, ID:", fileID)

	for {
		time.Sleep(2 * time.Minute)

		resp, err := api.vtClient.GetReport(fileID)
		if err != nil {
			log.Printf("err: %v, filename: %s\n", err, filename)
			return
		}

		if resp.Data.Attributes.Status != "completed" {
			log.Println(resp.Data.Attributes.Status)
			continue
		}

		malicious := resp.Data.Attributes.Stats["malicious"]
		suspicious := resp.Data.Attributes.Stats["suspicious"]
		harmless := resp.Data.Attributes.Stats["harmless"]

		if malicious != 0 || suspicious != 0 || harmless != 0 {
			message := fmt.Sprintf(
				"A virus was detected 🦠\n\nFilename: `%s`\n**%d** "+
					"malicious\n**%d** suspicious\n**%d** harmless\n\n", filename, malicious, suspicious, harmless,
			)

			for k, v := range resp.Data.Attributes.Results {
				if v.Category == "malicious" || v.Category == "suspicious" || v.Category == "harmless" {
					message += fmt.Sprintf("%s: `%s`\n", k, v.Result)
				}
			}

			if err := api.bot.SendMessage(message); err != nil {
				log.Printf("err: %v, filename: %s\n", err, filename)
				return
			}
		} else {
			message := fmt.Sprintf("No viruses are detected in the `%s` 💊", filename)

			if err := api.bot.SendMessage(message); err != nil {
				log.Printf("err: %v, filename: %s\n", err, filename)
				return
			}
		}
		break
	}
}
