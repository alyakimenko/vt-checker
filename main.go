package main

import (
	"flag"
	"log"

	"gitlab.com/alyakimenko/vt-checker/api"
	"gitlab.com/alyakimenko/vt-checker/config"
	"gitlab.com/alyakimenko/vt-checker/telegram"
	"gitlab.com/alyakimenko/vt-checker/workers"
)

func main() {
	configPath := flag.String("c", "config.json", "Path to config file.")
	flag.Parse()

	cfg, err := config.ParseConfigFile(*configPath)
	if err != nil {
		log.Fatal(err)
	}

	bot, err := telegram.New(cfg.TelegramBotToken, cfg.ChatID)
	if err != nil {
		log.Fatal(err)
	}

	app, err := api.New(cfg, bot)
	if err != nil {
		log.Fatal(err)
	}

	go workers.StartWorker(cfg, false)
	go workers.ListenBotUpdated(cfg, bot.Bot)

	log.Fatal(app.Start())
}
