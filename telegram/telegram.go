package telegram

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

type Bot struct {
	Bot *tgbotapi.BotAPI
	chatID int64
}

func New(token string, chatID int64) (*Bot, error) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		return nil, err
	}

	return &Bot{
		Bot: bot,
		chatID: chatID,
	}, nil
}

func (b *Bot) SendMessage(messageText string) (error) {
	msg := tgbotapi.NewMessage(b.chatID, messageText)
	msg.ParseMode = tgbotapi.ModeMarkdown

	_, err := b.Bot.Send(msg)
	return err
}
